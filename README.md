## Vision 


### A modern build cycle

* Gulp, hugulp, webpack? 


### Designed for Hugo theme overrides


### Implements

* layout: 12 column responsive grid system
* Type: good looking, basic type and style guide
* Responsive grid, with attention to navigation and advances for tablets
* Forms styling
* buttons styling
* basic blocks


### Uses Hugo for free

* Opengraph
* tw cards
* scheme? 
* sitemap
* rss
* overrides for analytics w/ GTM
* "google news templte"
* 



```
cd themes
git submodule add https://gitlab.com/neotericdesign-tools/hugo-boilerkit.git
```
